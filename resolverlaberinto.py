import sys
import numpy 
import cv2 as cv
from PIL import Image,ImageColor,ImageDraw
from tkinter import filedialog


class Nodos:
		#inicializo los objetos con su posición respectiva a la imagen.
	def __init__(self,fila,col):
		self.coordenadas = [fila,col]

		#inicializo las variables con -1 para que si no hay nada en alguna de las direcciones las variables ya esten creadas y no se rompa el codigo.

	derecha = -1	 #Guarda la posicion del nodo que haya a la derecha, si es que hay.
	izquierda = -1	 #Guarda la posicion del nodo que haya a la izquierda, si es que hay.
	arriba = -1		 #Guarda la posicion del nodo que haya a la arriba, si es que hay.
	abajo = -1 		 #Guarda la posicion del nodo que haya a la abajo, si es que hay.
	costo_izq = -1 	 #Guarda el costo de moverse hacia el nodo que se encuentra a la izquierda, si es que hay alguno.
	costo_der = -1 	 #Guarda el costo de moverse hacia el nodo que se encuentra a la derecha, si es que hay alguno.
	costo_arr = -1 	 #Guarda el costo de moverse hacia el nodo que se encuentra arriba, si es que hay alguno.
	costo_abj = -1 	 #Guarda el costo de moverse hacia el nodo que se encuentra abajo, si es que hay alguno.
	nodo_derecha = -1
	nodo_izquierda = -1
	nodo_arriba = -1
	nodo_abajo = -1
	costo = -1	 #Guarda el costo para llegar al nodo desde el inicio
	via_costo = -1 #Guarda las coordenadas del camino cuyo costo esta guardado en la variable "costo"

		#Crea la concxion entre si mismo y el nodo que este a la izquierda. Recibe como parametro la instancia de la clase nodo que se encuentre a la izquierda.

	def conexion_izquierda(self, n):
		self.nodo_izquierda = n
		self.izquierda = n.coordenadas
		self.costo_izq = self.coordenadas[1] - n.coordenadas[1]
		n.conexion_derecha(self)


		#Crea la concxion entre si mismo y el nodo que este a la derecha. Recibe como parametro la instancia de la clase nodo que se encuentre a la derecha.

	def conexion_derecha(self, n):
		self.nodo_derecha = n
		self.derecha = n.coordenadas
		self.costo_der = n.coordenadas[1] - self.coordenadas[1]


		#Crea la concxion entre si mismo y el nodo que este arriba. Recibe como parametro la instancia de la clase nodo que se encuentre a la arriba.

	def conexion_arriba(self, n):
		self.nodo_arriba = n
		self.arriba = n.coordenadas
		self.costo_arr = self.coordenadas[0] - n.coordenadas[0]
		n.conexion_abajo(self)		


		#Crea la concxion entre si mismo y el nodo que este abajo. Recibe como parametro la instancia de la clase nodo que se encuentre abajo.

	def conexion_abajo(self, n):
		self.nodo_abajo = n
		self.abajo = n.coordenadas
		self.costo_abj = n.coordenadas[0] - self.coordenadas[0]


		# Para cada conexion adyacente al nodo almacena el costo de llegar hasta alli utilisando este nodo como ruta. Solo almacena este nodo como camino si el nodo adyacente no cuenta con otro camino ya guardado o si el costo a traves de este nodo es menor.

	def seleccion_camino(self):
		
		if self.nodo_abajo != -1:
			if self.nodo_abajo.costo == -1:
				self.nodo_abajo.costo = (self.costo + self.costo_abj)
				self.nodo_abajo.via_costo = self
			elif self.nodo_abajo.costo > (self.costo + self.costo_abj):
				self.nodo_abajo.costo = (self.costo + self.costo_abj)
				self.nodo_abajo.via_costo = self

		if self.nodo_arriba != -1:
			if self.nodo_arriba.costo == -1:
				self.nodo_arriba.costo = (self.costo + self.costo_arr)
				self.nodo_arriba.via_costo = self
			elif self.nodo_arriba.costo > (self.costo + self.costo_arr):
				self.nodo_arriba.costo = (self.costo + self.costo_arr)
				self.nodo_arriba.via_costo = self

		if self.nodo_izquierda != -1:
			if self.nodo_izquierda.costo == -1:
				self.nodo_izquierda.costo = (self.costo + self.costo_izq)
				self.nodo_izquierda.via_costo = self
			elif self.nodo_izquierda.costo > (self.costo + self.costo_izq):
				self.nodo_izquierda.costo = (self.costo + self.costo_izq)
				self.nodo_izquierda.via_costo = self

		if self.nodo_derecha != -1:
			if self.nodo_derecha.costo == -1:
				self.nodo_derecha.costo = (self.costo + self.costo_der)
				self.nodo_derecha.via_costo = self
			elif self.nodo_derecha.costo > (self.costo + self.costo_der):
				self.nodo_derecha.costo = (self.costo + self.costo_der)
				self.nodo_derecha.via_costo = self

		#Crea una linea entre el nodo y el nodo anterior(el nodo al que se conecta para formar el camino. El segundo nodo esta almacenado en la variable "via_costo")		

	def dibujar_camino(self, draw):
		
		if self.via_costo != -1:
			draw.line([(self.coordenadas[1]*escala+escala /2),(self.coordenadas[0]*escala+escala /2),(self.via_costo.coordenadas[1]*escala+escala /2),(self.via_costo.coordenadas[0]*escala+escala /2)],(255,0,0),round(tamano/(tamano/escala)/10))
			draw.ellipse([self.via_costo.coordenadas[1]*escala+escala /2-round(tamano/(tamano/escala)/10),self.via_costo.coordenadas[0]*escala+escala /2-round(tamano/(tamano/escala)/10),self.via_costo.coordenadas[1]*escala+escala /2+round(tamano/(tamano/escala)/10),self.via_costo.coordenadas[0]*escala+escala /2+round(tamano/(tamano/escala)/10)], fill=(255,0,0))
		return self.via_costo
		
		
#sys.setrecursionlimit(1350)	
debug = False	#Muestra informacion extra en la consola en caso de ser verdadero.

	# Hago que el usuario ingrese la ruta de la imagen que contiene el laberinto. Este debe estar dibujado por lineas de un pixel de espesor y debe estar en blanco y negro. La primera y ultima filas del laberinto solo deben contener como punto en blanco el punto de incio t fin del laberinto respectivamente. 
ruta_imagen = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("png file","*.png*"),("jpeg files","*.jpg"))) 

nodos = [] #inicializo la variable que luego contendra la matriz con el laberinto obtenido de la imagen que se le cargo.

escala = 100 #Factor por el que se escalara la imagen para mostrarla.

im = Image.open(ruta_imagen) #Vuelvo a cargar la imagen pero esta vez para mostrarsela luego al usuario.

if debug:
	im.show()
	print("Modo de la imagen Cargada: "+str(im.mode))


	#Convierto la imagen cargada en una matriz de valores entre 0 y 255.
matrix = numpy.asarray(cv.imread(ruta_imagen,0)).tolist()
tamano = escala * len(matrix)
im = im.convert('RGB')
im2 = im.resize((tamano,tamano), Image.BOX)
if debug:
	im2.show()

#Obtengo el nodo de inicio buscando la posicion del nodo que se encuentra en la primer fila. 
for w in range(0,len(matrix[0])):
	if matrix[0][w] != 0:
		pos_inicio = (0,w)
		break

#Obtengo el nodo que representa el fin del laberinto buscando la posicion del nodo que se encuentre en la ultima fila.
for w in range(0,len(matrix[len(matrix)-1])):
	if matrix[len(matrix[len(matrix)-1])-1][w] != 0:
		pos_fin = (len(matrix)-1,w)
		break

if debug:
	print("Foto convertida en matriz: ")
	print(matrix)
	print("\nPosicion inicio: " + str(pos_inicio))
	print("Posicion fin: " + str(pos_fin))

	boolmtx = [] # Inicializo la variable que contendra la matriz booleana que representa el laberinto.
		# Convierto la matriz original en una matriz booleana que se guarda en boolmtx.
	for z in range(0,len(matrix)):
		tmp = [] #Creo una lista temporal para poder agregar las filas completas de la matriz en una misma posicion de boolmtx. 
		for w in range(0,len(matrix[z])): #Convierto los valores de la matriz original a 1s y 0s.
			if matrix[z][w] == 255:
				tmp.append(1)
			else:
				tmp.append(0)
		boolmtx.append(tmp)
	print("Laberinto en forma booleana: ")
	for s in boolmtx: # Imprimo la matriz de 0s y 1s.
		print(s)


	# Convierto la matriz a valores booleanos.
for x in range(0,len(matrix)):
	for z in range(0,len(matrix[x])):
		if(matrix[x][z] == 255):
			matrix[x][z] = True
		else:
			matrix[x][z] = False

if debug:
	print("Matriz convertida: ")
	print(matrix)

	# Recorro la matriz reconociendo los puntos clave donde debo colocar nodos.

for fila in range(0,len(matrix)):
	for col in range(0,len(matrix[fila])):
		bandera = False  # Mantengo un registro de si en esta vuelta se agrego algun nodo.
			# Corroboro si es el inicio de una linea hotizontal.
		if col == 0 and matrix[fila][col]: 
			inicio = True
		elif matrix[fila][col-1] == False and matrix[fila][col]:
			inicio = True
		else:
			inicio = False
			#Corroboro si es el fin de una linea horizontal.
		if col == len(matrix[fila])-1:
			if matrix[fila][col]: 
				fin = True
			else:
				fin = False
		elif matrix[fila][col+1] == False and matrix[fila][col] :
			fin = True
		else:
			fin = False

			#Corroboro si desde esa posicion es posible moverse hacia abajo.
		if fila < len(matrix)-1:
			if matrix[fila+1][col]:
				abajo = True
			else:
				abajo = False
		else:
			abajo = False

			#Corroboro si desde esa posicion es posible moverse hacia arriba.
		if fila == 0:
			arriba = False
		else:
			if matrix[fila-1][col]:
				arriba = True
			else:
				arriba = False

			# Utilizando las variables de arriba y abajo evaluo si la posivion se trata de un pasillo vertical.
		if arriba and abajo:
			pasillo_ver = True
		else:
			pasillo_ver = False

			# Utilizando las variables de arriba y abajo evaluo si se puede mover para alguno de esos lados sin tratarse de un pasillo vertical. 
		if (arriba ^ abajo):
			arriba_o_abajo = True
		else:
			arriba_o_abajo = False

			# Coloco un nodo en la posicion si se trata de un inicio o un final solo si cumple una de las dos condiciones de forma unica. 
		if (inicio ^ fin) and matrix[fila][col]:			
			nodos.append(Nodos(fila,col))
			bandera = True

			# Coloco un nodo en la posicion en caso de que sea inicio y final a la vez solo si no se trata de un pasillo vertical.
		elif (inicio | fin) & (~pasillo_ver) & matrix[fila][col]:
			nodos.append(Nodos(fila,col))
			bandera = True

		elif ((~(inicio|fin))& pasillo_ver) and matrix[fila][col]:
			nodos.append(Nodos(fila,col))
			bandera = True
			# Coloco un nodo en la posicion si se cumple la condiciond de arriba o abajo, es decir si es posible moverse hacia arriba o abajo exeptuando los casos donde se trate de un pasillo vertical.
		elif arriba_o_abajo and matrix[fila][col]:
			nodos.append(Nodos(fila,col))
			bandera = True

			# Si no se trata de un nodo de incio, conecto este nodo con el anteror ya que como se recorre la matriz de arriba a abajo y de izquierda a derecha, el nodo en la lista que se encuentre anterior al actual sera el que este a su izquierda. 
		if (not inicio) and (len(nodos)>1) and bandera: 
				nodos[len(nodos)-1].conexion_izquierda(nodos[len(nodos)-2])

			# Si es posible moverse hacia arriba del nodo actual, busco cual es el primer nodo con el que me encuentro si voy hacia arriba para realizar la conexion entre estos. Para buscarlo, recorro la lista de nodos de forma inversa hasta encontrar uno que se encuentre en la misma posicion de "x"(misma columna).
		if arriba and bandera:
			for h in range(len(nodos)-2,-1,-1):
				if nodos[h].coordenadas[1] == col:
					nodos[len(nodos)-1].conexion_arriba(nodos[h])
					break

if debug:
	print("Nodos y conexiones: ")
for x in range(0,len(nodos)):
	if debug:
		print("Posicion nodo: "+str(nodos[x].coordenadas) + "	Posicion nodo izquierda: " + str(nodos[x].izquierda) + "	Costo nodo izquierda: " + str(nodos[x].costo_izq) + "	Posicion nodo derecha: " + str(nodos[x].derecha) + "	Costo derecha: " + str(nodos[x].costo_der) + "	Posicion nodo arriba: " + str(nodos[x].arriba) + "	Costo arriba: " + str(nodos[x].costo_arr) + "	Posicion nodo abajo: " + str(nodos[x].abajo) + "	Costo abajo: " + str(nodos[x].costo_abj))
	#print(nodos[x].coordenadas)
	im.putpixel((nodos[x].coordenadas[1],nodos[x].coordenadas[0]), (0,255,0))

if debug: 
	print("Modo de la imagen luego de la conversion a 'RGB': "+str(im.mode))

	#Reescalo la imagen para que sea nas facuknebte visible y para poder dibujar las lineas de conxion entre los nodos.
im = im.resize((tamano,tamano),Image.BOX)
	# Creo un objeto dibujo a partir de la imagen para poder trazar los caminos y conexiones en ella.
draw = ImageDraw.Draw(im)

	# Recorro la lista de nodos para dibujar las lineas de conxion entre ellos.
for z in nodos:
	if z.derecha != -1:
		draw.line([(z.coordenadas[1]*escala+escala /2),(z.coordenadas[0]*escala+escala /2),(z.derecha[1]*escala+escala /2),(z.derecha[0]*escala+escala /2)],(0,255,0),round(tamano/(tamano/escala)/10))

	if z.abajo != -1:
		draw.line([(z.coordenadas[1]*escala+escala /2),(z.coordenadas[0]*escala+escala /2),(z.abajo[1]*escala+escala /2),(z.abajo[0]*escala+escala /2)],(0,255,0),round(tamano/(tamano/escala)/10))

im.save("Cambiado.png",optimize=True, quality=100)
im.show()
#Encontrar el camino:

priority_queue = nodos  # Copio la lista de nodos a otra lista para podes realizar el Dijkstra's Algorithm

priority_queue[0].costo = 0 #Hago que el costo del nodo de inicio sea 0

if debug:
	print("Cantidad Inicial de nodos: " + str(len(nodos)))
	print("Coordenadas del nodo en la posicion 0 de la lista: " + str(priority_queue[0].coordenadas) + "Costo nodo abajo: " + str(priority_queue[0].nodo_abajo.costo))

	# Mientras el nodo del final no tenga un costo definido, es decir, mientras no hayamos encontrado un camino que llegue hasta el, vamos a seguir iterando el algoritmo.
while nodos[len(nodos)-1].costo == -1:
	
	#Ejecutamos seleccion camino en el nodo de la lista cuyo costo desde el inicio sea menor (Debio al ordenamiento que se realiza al final de cada iteracion de este while, el nodo de cuyo costo sea menor se encontrara siempre al principio de la lista)
	
	priority_queue[0].seleccion_camino()

	temp = []  # Creo una lista temporal para poder eliminar el elemento de la lista que ya no nescesitare mas. (Tengo que hacerlo de esta forma y no con la funcion del() ya que esta destruye el objeto completamente por lo que tambien lo pierdo en la lista de nodos lo que volveria imposible encontrar el camino al final).
		# Copio todos los elementos de priority_queue a la lista temporal exeptuando el primero ya que este ya fue utilizado.
	for h in range(1,len(priority_queue)):
		temp.append(priority_queue[h])
	priority_queue = temp # Copio la lista temporal a priority_queue. 
		# A partir de aca se encuentra el codigo necesario para reordenar la lista de forma que los caminos menos costosos queden adelante.
		# Como los nodos cuyo costo es desconocido aun llevan -1 en el atributo costo para representar que aun no se conoce, no se puede realizar burbujeo directamenta ya que este dejaria todos los nodos de los que no se conoce el costo al principio de la lista que es exactameente lo opuesto a lo que se desea. Para solucionar este problema primero debo mover todos los elementos cuyo costo sea -1 al final de la lista y realizar burbujeo sobre el resto de los elementos.
		# Para dejar todos los elementos con costo igual a -1 al final de la lista recorro la lista desde ambos extremos a la vez intercambiando los elementos que esten del lado equivocado. 
	pos_indef = 0  # Inicializo la posicion del primer valor indefinido(-1). Esto es con el mero motivo de inicializarla ya que el valor real lo recibira unas lineas abajo cuando el for recorra la lista de forma ascendente. 
	pos_def = len(priority_queue)-1  # Inicializo la posicion del primer valor definido( != -1). Esto es con el mero motivo de inicializarla ya que el valor real lo recibira unas lineas abajo cuando el for recorra la lista de forma descendente. 
		# Repito el intercambio de valores tantas veces como elementos haya en la lista de forma de que en el peor de los casos que todos se encuentren mal ubicados la lista igualmente termine bien ordenada. 
	for z in range(0,len(priority_queue)):

		
			# Recorro la lista en forma descendente buscando la posicion del ultimo valor definido ( != -1).
		for w in range(len(priority_queue)-1,-1,-1):
			if priority_queue[w].costo != -1:
				if debug:
					print("Pos def: " + str())
				pos_def = w
				break
			# Recorro la lista en forma ascendente buscando la posicion del primer valor indefinido ( == -1).
		for w in range(0,len(priority_queue)):
			if priority_queue[w].costo == -1:
				if debug:
					print("Pos indef: "+ str(w))
				pos_indef = w
				break
			# Si el primer ultimo elemento con costo definido esta despues del ultimo valor definido significa que la lista aun se encuentra desordenada por lo que intercambio los elementos en dichas posiciones con el objetivo de ordenarla.
		if pos_def > pos_indef:
			aux = priority_queue[pos_indef]
			priority_queue[pos_indef] = priority_queue[pos_def]
			priority_queue[pos_def] = aux
		else:  # En cambio si el primer elemento con costo indefinido se encuentra luego del ultimo con costo definido significa que la lista ya se encuentra ordenada por lo que rompemos el bucle for para evitar iteraciones innescesarias. 
			break
	if debug:
		print("_________________")
		for p in priority_queue:
			print("Coordenada: " + str(p.coordenadas) + "Costo: " + str(p.costo)+" ")
		# Una vez que ya separamos los elementos con costo definido de los que tienen costo indefinido procedemos a realizar burbujeo sobre los elementos definidos con el objetivo de encontrar el camnino que, por el momento, representa el menor costo.
	for z in range(0, pos_indef):  # Notece que el for solo recorre la lista hasta pos_indef ya que esta es la posicion a partir de la cual se encuentran los elementos con costo indefinido. 
		for w in range(0, (pos_indef - z)-1):
			if priority_queue[w].costo > priority_queue[w+1].costo:
				aux = priority_queue[w+1]
				priority_queue[w+1] = priority_queue[w]
				priority_queue[w] = aux

if debug:
	print("Cantidad elementos en la lista nodos: " + str(len(nodos)))
	print("Coordenadas del ultimo elemento de la lista nodos: " + str(nodos[len(nodos)-1].coordenadas))
	print("Coordenadas antepenultimo nodo: " + str(nodos[len(nodos)-1].via_costo.via_costo.coordenadas))

	
nodo_dibujo = nodos[len(nodos)-1].dibujar_camino(draw)

while nodo_dibujo != -1:
	print(str(nodo_dibujo) + str(nodo_dibujo.coordenadas))
	nodo_dibujo = nodo_dibujo.dibujar_camino(draw)

# Muestro la imagen con las lineas ya dibujadas.
im.save("Resuelto.png",optimize=True, quality=100)
im.show()
